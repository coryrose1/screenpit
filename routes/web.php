<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// TV Search
Route::get('/search/{keyword}', 'ShowsController@search')->name('tv.search');
Route::post('/store', 'ShowsController@store')->name('tv.store')->middleware('auth');

// User
Route::get('/users');
Route::get('/users/fetch-shows', 'UsersController@fetchShows');
Route::get('/users/fetch-all-shows', 'UsersController@fetchAllShows');

Route::get('/users/authenticated', 'UsersController@fetchAuthenticated');
Route::get('/users/delete/{id}', 'ShowsController@destroy')->middleware('auth');

//Library
Route::view('/library', 'library')->name('library')->middleware('auth');
Route::get('/library/fetch-calendar-shows', 'UsersController@fetchMyCalendarShows')->middleware('auth');
Route::get('/library/fetch-weekly-episodes', 'UsersController@fetchWeeklyEpisodes')->middleware('auth');

//Calendar
Route::view('/calendar', 'calendar')->name('calendar')->middleware('auth');

Route::get('/test', 'TestController@test')->name('tv.test');
