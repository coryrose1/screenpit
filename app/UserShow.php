<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserShow extends Model
{
    protected $table = 'user_shows';

    protected $fillable = ['user_id', 'show_id', 'currently_watching'];

    public function show()
    {
        return $this->belongsTo(Show::class);
    }

    public function episodes()
    {
        return $this->hasManyThrough(Episode::class, Show::class, 'id', 'show_id');
    }
}
