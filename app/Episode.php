<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Episode extends Model
{
    protected $table = 'episodes';

    protected $fillable = ['tvmaze_id', 'show_id', 'show', 'name', 'summary', 'season', 'number',
        'airdate', 'airtime', 'runtime', 'image'];

    public function show()
    {
        return $this->belongsTo(Show::class);
    }

    public function userShows()
    {
        return $this->belongsToMany(UserShow::class);
    }

}
