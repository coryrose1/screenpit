<?php

namespace App\Http\Controllers;

use App\Episode;
use App\User;
use App\UserShow;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UsersController extends Controller
{
    public function fetchAuthenticated()
    {
        if (Auth::user())
        {
            $user = User::find(Auth::user()->id);
            return response()->json($user);
        }
        else
        {
            return response()->json('');
        }
    }

    public function fetchShows()
    {
        if (Auth::user())
        {
            $user_shows = UserShow::where('user_id', '=', Auth::user()->id)
                ->with('show', 'episodes')->get();
            return response()->json($user_shows);
        }
        else
        {
            return response()->json('');
        }
    }

    public function fetchAllShows()
    {
            $user_shows = UserShow::with('show', 'episodes')->get();
            return response()->json($user_shows);

    }

    public function fetchMyCalendarShows()
    {
        $my_shows = UserShow::with('episodes')
            ->where('user_id', '=', Auth::user()->id)
            ->get();
        $events = [];

        foreach ($my_shows as $my_show){
            foreach ($my_show->episodes as $episode){
                if ($episode->airdate >= Carbon::now()->subYear()){
                    $airdate = new Carbon($episode->airdate);
                    $airdate = $airdate->toDateTimeString();
                $events[] = [
                    'title' => $episode->show,
                    'start' => $airdate,
                    'name' => $episode->name,
                    'summary' => $episode->summary,
                    'season' => $episode->season,
                    'number' => $episode->number
                ];
                }
            }
        }

        return response()->json($events);
    }

    public function fetchWeeklyEpisodes()
    {

        $airdates = DB::table('user_shows')
            ->join('episodes', 'user_shows.show_id', '=' ,'episodes.show_id')
            ->select(DB::raw('DATE(episodes.airdate) as airdate'))
            ->where('user_shows.user_id', '=', Auth::user()->id)
            ->whereBetween('episodes.airdate', [Carbon::now()->startOfDay(), Carbon::now()->addWeek()->startOfDay()])
            ->groupBy('episodes.airdate')
            ->get();
        $episodes = DB::table('user_shows')
            ->join('episodes', 'user_shows.show_id', '=' ,'episodes.show_id')
            ->where('user_shows.user_id', '=', Auth::user()->id)
            ->whereBetween('episodes.airdate', [Carbon::now(), Carbon::now()->addWeek()])
            ->get();

        $i = 0;
        foreach ($airdates as $airdate)
        {
            $timeline[] = [$airdate->airdate => []];
            foreach ($episodes as $episode)
            {
                $episode_airdate = new Carbon($episode->airdate);
                $episode_airdate = $episode_airdate->toDateString();

                if ($airdate->airdate == $episode_airdate)
                {
                    $timeline[$i][$airdate->airdate][] = $episode;
                }
            }
            $i++;
        }

        return response()->json($timeline);
    }

}
