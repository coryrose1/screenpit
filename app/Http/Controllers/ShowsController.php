<?php

namespace App\Http\Controllers;

use App\Jobs\QueueRetrieveEpisodes;
use App\Jobs\QueueShowUpdateAirdates;
use App\Jobs\QueueTmdbShowSearch;
use App\Jobs\QueueUpdateNetworks;
use Carbon\Carbon;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use App\Show;
use App\UserShow;
use Illuminate\Http\Request;
use JPinkney\TVMaze\Client;
use Tmdb\Laravel\Facades\Tmdb;

class ShowsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $show = Show::updateOrCreate(
            ['tvmaze_id' => $request->tvmaze_id],
            [
            'title' => $request->title,
            'summary' => $request->summary,
            'network' => $request->network,
            'runtime' => $request->runtime,
            'image' => $request->image,
            'premiered' => $request->premiered,
            'status' => $request->status,
            'average' => $request->average,
        ]);

        $show->save();

        QueueTmdbShowSearch::dispatch($show);
        QueueRetrieveEpisodes::dispatch($show)
            ->delay(Carbon::now()->addSeconds(1));
        QueueShowUpdateAirdates::dispatch($show)
            ->delay(Carbon::now()->addSeconds(2));
        QueueUpdateNetworks::dispatch()
            ->delay(Carbon::now()->addSeconds(3));


        $user_show = UserShow::updateOrCreate(
            [
                'user_id' => $user->id,
                'show_id' => $show->id
                ]
        );

        $user_show->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $show = UserShow::where('user_id', '=', Auth::user()->id)
            ->where('show_id', '=', $id)
            ->first();
        $show->delete();
        return redirect()->route('library');

    }

    public function search($keyword){
        $Client = new Client();
        $results = $Client->TVMaze->search($keyword);

        return response()->json($results);
    }
}
