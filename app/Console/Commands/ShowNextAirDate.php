<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Show;
use Carbon\Carbon;

class ShowNextAirDate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shows:airdate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will update next_airdate and next_airtime on shows';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $shows = Show::with('episodes')->get();
        $today = Carbon::now()->toDateString();
        foreach ($shows as $show){
            foreach ($show->episodes as $episode){
                if ($episode->airdate >= $today){
                    $show->next_airdate = $episode->airdate;
                    $show->next_airtime = $episode->airtime;
                    $show->save();
                    break;
                }
                continue;
            }
        }
    }
}
