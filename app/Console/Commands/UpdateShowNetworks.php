<?php

namespace App\Console\Commands;

use App\Show;
use Illuminate\Console\Command;
use Tmdb\Laravel\Facades\Tmdb;

class UpdateShowNetworks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shows:networks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Replaces empty show networks';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $shows = Show::whereNull('network')->get();
        foreach ($shows as $show){
            $search = Tmdb::getTvApi()->getTvShow($show->tmdb_id);
            if (count($search['networks']) > 0){
                $show->network = $search['networks'][0]['name'];
                $show->save();
            }
        }
    }
}
