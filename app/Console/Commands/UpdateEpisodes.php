<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Show;
use Carbon\Carbon;
use App\Jobs\QueueRetrieveEpisodes;

class UpdateEpisodes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'episodes:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the episodes for all shows';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $shows = Show::get();

        foreach ($shows as $show)
        {
            QueueRetrieveEpisodes::dispatch($show)->delay(Carbon::now()->addSeconds(10));
        }
    }
}
