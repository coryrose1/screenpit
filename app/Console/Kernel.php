<?php

namespace App\Console;

use App\Console\Commands\ShowNextAirDate;
use App\Console\Commands\UpdateEpisodes;
use App\Console\Commands\UpdateShowNetworks;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        ShowNextAirDate::class,
        UpdateEpisodes::class,
        UpdateShowNetworks::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
         $schedule->command('shows:airdate')
                  ->daily();
        $schedule->command('episodes:update')
            ->daily();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
