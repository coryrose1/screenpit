<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Tmdb\Laravel\Facades\Tmdb;
use App\Show;

class QueueTmdbShowSearch implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $show;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($show)
    {
        $this->show = $show;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $result = Tmdb::getSearchApi()->searchTv($this->show->title);
        $existingShow = Show::find($this->show->id);
        foreach ($result['results'] as $result)
        {
            if ($result['name'] == $existingShow->title){
                $showInfo = Tmdb::getTvApi()->getTvshow($result['id']);
                    $existingShow->tmdb_id = $showInfo['id'];
                    $existingShow->num_episodes = $showInfo['number_of_episodes'];
                    $existingShow->num_seasons = $showInfo['number_of_seasons'];
                    $existingShow->backdrop = $showInfo['backdrop_path'];
            }
        }
        $existingShow->save();
    }
}
