<?php

namespace App\Jobs;

use App\Show;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Carbon\Carbon;

class QueueShowUpdateAirdates implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $show;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($show)
    {
        $this->show = $show;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $show = Show::with('episodes')->find($this->show->id);
        $today = Carbon::now()->toDateString();
            foreach ($show->episodes as $episode) {
                if ($episode->airdate >= $today) {
                    $show->next_airdate = $episode->airdate;
                    $show->save();
                    break;
                }
                continue;
            }
    }
}
