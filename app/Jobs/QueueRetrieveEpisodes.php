<?php

namespace App\Jobs;

use App\Episode;
use Carbon\Carbon;
use JPinkney\TVMaze\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class QueueRetrieveEpisodes implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $show;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($show)
    {
        $this->show = $show;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $Client = new Client();
        $episodes = $Client->TVMaze->getEpisodesByShowID($this->show->tvmaze_id);

        foreach ($episodes as $episode) {

                if ($episode->airtime) {
                    $airtime = $episode->airtime;
                } else {
                    $airtime = '00:00:00';
                }

            $airdate = $episode->airdate.' '.$episode->airtime;
            $airdate = new Carbon($airdate);
            $airdate = $airdate->toDateTimeString();



            $newEpisode = Episode::updateOrCreate(
                ['tvmaze_id' => $episode->id,
                    'image' => $episode->originalImage,
                    'summary' => $episode->summary,
                    'airdate' => $airdate],
                [
                    'show_id' => $this->show->id,
                    'show' => $this->show->title,
                    'name' => $episode->name,
                    'season' => $episode->season,
                    'number' => $episode->number,
                    'runtime' => $episode->runtime
                ]);

            $newEpisode->save();
        }
    }
}
