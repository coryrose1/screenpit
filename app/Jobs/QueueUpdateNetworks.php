<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Show;
use Tmdb\Laravel\Facades\Tmdb;

class QueueUpdateNetworks implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $shows = Show::whereNull('network')->get();
        foreach ($shows as $show){
            $search = Tmdb::getTvApi()->getTvShow($show->tmdb_id);
            if (count($search['networks']) > 0){
                $show->network = $search['networks'][0]['name'];
                $show->save();
            }
        }
    }
}
