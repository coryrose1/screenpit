<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Show extends Model
{
    protected $table = 'shows';

    protected $fillable = [
        'tvmaze_id', 'tmdb_id', 'title', 'summary', 'network', 'runtime',
        'image', 'premiered', 'status', 'average', 'num_seasons', 'num_episodes',
        'next_airdate', 'next_airtime'
    ];

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function episodes()
    {
        return $this->hasMany(Episode::class);
    }

    public function userShows()
    {
        return $this->hasMany(UserShow::class);
    }
}
