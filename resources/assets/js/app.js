
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
window.moment = require('moment');


var VueScrollTo = require('vue-scrollto');
import FullCalendar from 'vue-full-calendar';
Vue.use(FullCalendar);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'));
Vue.component('navbar', require('./components/Navbar.vue'));
Vue.component('hero', require('./components/Hero.vue'));
Vue.component('calendar', require('./components/Calendar.vue'));
Vue.component('library', require('./components/library/Library.vue'));
Vue.component('library-overview', require('./components/library/overview/Overview.vue'));
Vue.component('episode-timeline', require('./components/library/overview/EpisodeTimeline.vue'));
Vue.component('episode-summary', require('./components/library/overview/EpisodeSummary.vue'));
Vue.component('my-shows', require('./components/library/MyShows.vue'));
Vue.component('all-shows', require('./components/library/AllShows.vue'));
Vue.component('show-info', require('./components/library/ShowInfo.vue'));
Vue.component('show-card', require('./components/library/ShowCard.vue'));
Vue.component('notification', require('./components/Notification.vue'));
Vue.component('tv-search-results', require('./components/tv/TvSearchResults.vue'));
Vue.component('tv-search-form', require('./components/tv/TvSearchForm.vue'));
Vue.component('tv-add-button', require('./components/tv/TvAddButton.vue'));

const app = new Vue({
    el: '#app',
    data: {
        user: null,
        isGuest: true,
    },
    created: function() {
        const vm = this;
        axios.get('/users/authenticated')
            .then( function (response) {
                vm.user = response.data;
                vm.user ? vm.isGuest = false : vm.isGuest = true;
            });
    }
});


Vue.use(VueScrollTo, {
    container: "body",
    duration: 500,
    easing: "ease",
    offset: 0,
    cancelable: true,
    onDone: false,
    onCancel: false,
    x: false,
    y: true
});
