@extends('layouts.app')
@section('content')
        <hero v-bind:unauthenticated="isGuest" v-bind:userdata="user"></hero>
    <section class="section">
        <div class="container">
        <div class="columns is-desktop is-vcentered">
            <div class="column has-text-centered">
                <i class="fa fa-search fa-5x"></i>
                <p class="title is-size-4">Find your shows</p>
            </div>
            <div class="column has-text-centered">
                <i class="fa fa-calendar-plus-o fa-5x"></i>
                <p class="title is-4">Create your calendar</p>
            </div>
            <div class="column has-text-centered">
                <i class="fa fa-users fa-5x"></i>
                <p class="title is-4">Share with friends</p>
            </div>
        </div>
        </div>
    </section>
    @endsection