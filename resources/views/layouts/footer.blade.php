<footer class="footer">
    <div class="container">
        <div class="content has-text-centered">
            <p>
                <strong>Screenpit</strong> by <a href="mailto:coryrosenwald@gmail.com">Cory Rosenwald</a>.
            <p>TV data provided courtesy of <a href="https://www.themoviedb.org/">TMDB</a> and <a href="https://www.tvmaze.com/">TVMaze</a>.</p>
            </p>
        </div>
    </div>
</footer>