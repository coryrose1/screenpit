<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/vue2-animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/hint.min.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">
    <navbar v-bind:user="user" v-bind:guest="isGuest"></navbar>
    <main class="main-content" style="position:relative">
    @yield('content')
    </main>
    <div class="push"></div>
</div>
@include('layouts.footer')

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
@include('layouts.scripts')
</body>
</html>