<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEpisodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('episodes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tvmaze_id')->unsigned();
            $table->integer('show_id')->unsigned();
            $table->foreign('show_id')->references('id')
                ->on('shows')->onDelete('cascade');
            $table->string('show')->nullable();
            $table->string('name')->nullable();
            $table->text('summary')->nullable();
            $table->integer('season')->unsigned()->nullable();
            $table->integer('number')->unsigned()->nullable();
            $table->datetime('airdate')->nullable();
            $table->integer('runtime')->unsigned()->nullable();
            $table->string('image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('episodes');
    }
}
