<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shows', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tvmaze_id')->unsigned();
            $table->integer('tmdb_id')->nullable()->unsigned();
            $table->string('title');
            $table->text('summary');
            $table->string('network')->nullable();
            $table->integer('runtime')->nullable()->unsigned();
            $table->string('image')->nullable();
            $table->string('backdrop')->nullable();
            $table->date('premiered')->nullable();
            $table->string('status')->nullable();
            $table->float('average')->nullable();
            $table->integer('num_seasons')->nullable()->unsigned();
            $table->integer('num_episodes')->nullable()->unsigned();
            $table->datetime('next_airdate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shows');
    }
}
